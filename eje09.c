#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main(){
	int c,i,j;

	printf("Ingrese las palabras a ordenar?: ");
	scanf("%i",&c);

	char dic[c][50];
	for(i=0; i<c;i++){
		printf("Palabra #%i: ",i+1);
		scanf("%49s",dic[i]);
	}

	char tmp[50];
	for(i=0;i<=c;i++){
		for(j=1;j<c;j++){
			if(strcmp(dic[j-1],dic[j]) < 0){
				strcpy(tmp,dic[j]);
				strcpy(dic[j], dic[j-1]);
				strcpy(dic[j-1],tmp);
			}
		}
	}

	for(i=0;i<c;i++){
		printf("%s\n",dic[i]);
	}
}

