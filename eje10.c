#include <stdio.h>
#include <string.h>

typedef struct {
	char nombre[40];
	int dui;
	float sueldo;
} empleado;

void imprimir(empleado[], int);
void oN(empleado[], int);
void oS(empleado[], int);
void pS(empleado[], int);

void main(){
	char control;

	int limite = 0;
	double promedio;
	empleado emp[1000];

	while(1){
		printf("ingrese el nombre del empleado: ");
		scanf("%s",emp[limite].nombre);
		printf("Ingrese el dui del empleado: ");
		scanf("%d",&(emp[limite].dui));
		printf("Ingrese el sueldo del empleado: ");
		scanf("%f",&(emp[limite].sueldo));

		limite++;
		printf("Continuar? Y/N ");
		scanf(" %c",&control);

		if(control == 'N' || control == 'n') break;
	}

	printf("\nOrden por nombre\n\n");
	oN(emp,limite);
	imprimir(emp,limite);

	printf("\nOrden por sueldo\n\n");
	oS(emp,limite);
	imprimir(emp,limite);

	pS(emp,limite);
}

void imprimir(empleado emp[],int limite){
	for(int i=0;i<limite;i++){
		printf("Empleado:\n\tNombre: %s\n\tDUI: %d\n\tSueldo: %f\n",
		emp[i].nombre,emp[i].dui,emp[i].sueldo);
	}
}

void oN(empleado emp[], int limite){
empleado tmp;

	for(int i=0;i<limite;i++){
		for(int j=1;j<limite;j++){
			if(strcmp(emp[j-1].nombre,emp[j].nombre)>0){
				tmp = emp[j];
				emp[j] = emp[j-1];
				emp[j-1] = tmp;
			}
		}
	}
}

void oS(empleado emp[], int limite){
empleado tmp;

	for(int i=0;i<limite;i++){
		for(int j=1;j<limite;j++){
			if(emp[j].sueldo > emp[j-1].sueldo){
				tmp = emp[j];
				emp[j] = emp[j-1];
				emp[j-1] = tmp;
			}
		}
	}
}

void pS(empleado emp[], int limite){
	double res = 0;
	for(int i=0;i<limite;i++){
		res+= emp[i].sueldo;
	}
	printf("El promedio de los sueldos es: %0.2f\n",res/limite);
}

